Empat project yang sudah dibuat:
1. Laravel + react (90%)
2. Laravel + react + inertia (10%)-ERROR(code lebih complek dan susah difahami)
3. Laravel + react + vue (5%)-ERROR(fungsi yang harusnya tersedia malah tidak tersedia pada version sekarang dan belum tahu pengganti fungsi tersebut)
4. Laravel + react + vite + breeze (30%)-ERROR(axios.get gagal walau code sudah benar) api sudah benar dikirim dari laravel, penangkapan melalui axios sudah benar, namun session tidak terbaca




# No 1
Mampu menunjukkan keseluruhan Use Case beserta ranking dari tiap Use Case dari produk digital:

Use case user
Use case manajemen perusahaan
Use case direksi perusahaan (dashboard, monitoring, analisis)

![use case](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/use_case_bjb.drawio.png)

#### Nasabah
| Key  | Use Case | Priority | Status |
| ------------- | ------------- |------------- |------------- |
| 1  | Nasabah ingin bisa login dengan akun pribadi | Tinggi | Completed |
| 2 | Nasabah ingin bisa transfer antar bank ke sesama bank  | Tinggi | Completed |
| 3 | Nasabah ingin bisa transfer antar bank ke beda bank  | Tinggi | Planned |
| 4 | Nasabah ingin bisa transfer wallet ke akun sesama wallet | Tinggi | Planned |
| 5 | Nasabah ingin bisa transfer wallet ke akun wallet yang berbeda  | Tinggi | Planned |
| 6 | Nasabah ingin bisa mengecek informasi tentang dirinya | Tinggi | Completed |
| 7 | Nasabah ingin bisa mengisi pulsa | Rendah | Unplanned |
| 8 | Nasabah ingin bisa membayar tagihan listrik | sedang | Planned |

#### Direktur
| Key  | Use Case | Priority | Status |
| ------------- | ------------- |------------- |------------- |
| 1  | Direktur ingin bisa login dengan akun pribadi | Tinggi | Planned |
| 2 | Direktur ingin bisa cek laporan data transaksi | Tinggi | Planned |
| 3 | Direktur ingin bisa validasi pembukuan bulanan  | Tinggi | Planned |
| 4 | Direktur ingin bisa mengecek informasi tentang dirinya | Tinggi | Planned |

#### Manager
| Key  | Use Case | Priority | Status |
| ------------- | ------------- |------------- |------------- |
| 1  | Manager ingin bisa login dengan akun pribadi | Tinggi | Planned |
| 2 | Manager ingin bisa input laporan data transaksi | Tinggi | Planned |
| 3 | Manager ingin bisa input pembukuan bulanan  | Tinggi | Planned |
| 4 | Manager ingin bisa mengecek informasi tentang dirinya | Tinggi | Planned |


PENJELASAN :


# No 2
Mampu mendemonstrasikan Class Diagram dari keseluruhan Use Case produk digital

![class diagram](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/class_diagram-class_diagram_bjb.drawio.png)

PENJELASAN:



# No 3
Mampu menunjukkan dan menjelaskan penerapan setiap poin dari SOLID Design Principle
 1. Single Responsibility Principle
![Single Responsibility](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/Single_Responbility.gif)

2. Open-Closed Principle
![Open-Closed](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/Open-Closed_.gif)

3. Interface Segregation Principle
![Interface Segregation](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/laravel_route.gif)

PENJELASAN:

SOLID
   - S-> Dalam Laravel, SRP berarti bahwa setiap kelas atau komponen harus bertanggung jawab atas satu hal tertentu dan memiliki fokus yang jelas dalam melakukan tugas-tugasnya. Dengan menerapkan SRP, kita dapat menghindari kelas yang terlalu kompleks dan terlalu banyak melakukan tugas yang berbeda. Pada class LoginController memiliki fungsi yang dikhususkan untuk autentikasi nasabah saat masuk menuju dashboard
-    O-> Pemisahan Logika Bisnis dengan Kontroler: Dalam Laravel, prinsip OCP juga dapat diterapkan dengan memisahkan logika bisnis dari kontroler. Kontroler berfungsi sebagai penghubung antara rute dan logika bisnis. Dengan memisahkan logika bisnis ke dalam kelas-kelas terpisah, Anda dapat memperluas perilaku aplikasi dengan menambahkan kelas-kelas baru tanpa mengubah kontroler yang telah ada. Pada class DashboardController, fitur bisa ditambahkan tanpa harus memodifikasi fungsi fungsi sebelumnya
-    I-> Dalam Laravel, terdapat banyak kelas dan fungsi bawaan yang disediakan oleh kerangka kerja tersebut untuk mempermudah pengembangan aplikasi. Laravel memisahkan fungsionalitas yang berbeda ke dalam kelas-kelas terpisah dan menyediakan antarmuka yang sesuai untuk berinteraksi dengan fungsionalitas tersebut. Dalam Laravel terdapat kelas seperti Route, DB, Auth, dan lain-lain, yang menyediakan fungsi-fungsi seperti get, post, insert, update, authenticate, dan sebagainya. Ketika Anda menggunakan framework Laravel, Anda dapat memanggil fungsi-fungsi ini sesuai dengan kebutuhan aplikasi Anda.



# No 4
Mampu menunjukkan dan menjelaskan Design Pattern yang dipilih

![Design Pattern MVC](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/MVC.gif)

PENJELASAN:
Jenis Design Pattern MVC, Memecah aplikasi menjadi tiga komponen utama: Model
(representasi data), View (tampilan pengguna), dan Controller (logika pengendali). Ini
memisahkan logika bisnis, tampilan, dan interaksi pengguna, memudahkan pengembangan
dan perubahan dalam aplikasi.


# No 5
Mampu menunjukkan dan menjelaskan konektivitas ke database

**Koneksi ke database**
![konektivitas ke database](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/konektifitas_env_database_BJB.gif)

**Salah satu contoh instansiasi pada Model**
![konektivitas Model](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/konektifitas_model_database_BJB.gif)

**Salah satu contoh Implementasi pada Controller**
![konektivitas controller](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/konektifitas_database_BJB.gif)

PENJELASAN:
Koneksi antara program dengan database di instansiasi pada file _ENV_. implementasi pada program ini, menggunakan mysql dengan host nya localhost. Konektifitas database biasanya berpusat pada Model, setelah model diidentifikasi, maka pengkoneksian database dibuat di tiap - tiap fungsi, seperti pada fungsi index(), di dalam fungsi tersebut ada query, juga bahasa SQL untuk menampilkan dan membuat data.


# No 6
Mampu menunjukkan dan menjelaskan pembuatan web service dan setiap operasi CRUD nya

**AUTH**
![web service Auth](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/web_service_Auth.gif)


**AJAX**
![web service Ajax](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/web_service_ajax.gif)


**CRUD**
![CRUD](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/CRUD.gif)


PENJELASAN:
Fungsi ajaxTransfer mengambil nomor rekening dari permintaan yang dikirim oleh klien, kemudian menggunakan nomor rekening tersebut untuk mencari pengguna yang sesuai dalam database menggunakan model User. Hasil pencarian pengguna kemudian dikembalikan dalam format JSON menggunakan metode response()->json().



# No 7
Mampu menunjukkan dan menjelaskan Graphical User Interface dari produk digital


- LOGIN & LOGOUT
![LOGIN & LOGOUT](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/login___logout_GUI.gif)

- TRANSFER SESAMA BANK
![TRANSFER SESAMA BANK](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/transfer_sesama_bank_GUI.gif)

- ADMIN PAGE (DIREKTUR)
![Admin Page](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/react_login_GUI.gif)

PENJELASAN:
Penjelasan tiap menu, seperti login: menampilkan form username dan password dll

# No 8
Mampu menunjukkan dan menjelaskan HTTP connection melalui GUI produk digital


#### Router React
![REACT JS](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/react_access.gif)

#### Connection
![CONNECTION REACT](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/web_service_react.gif)





# No 9
Mampu Mendemonstrsikan produk digitalnya kepada publik dengan cara-cara kreatif melalui video Youtube

#### Youtube 
[Youtube Kemal]()

# No 10
BONUS !!! Mendemonstrasikan penggunaan Machine Learning

![chatbot](https://gitlab.com/kemal.mkp/project-website-bjb/-/raw/main/chatbot.gif)

PENJELASAN:
Pembuatan fitur Chatbot dalam menangani permasalahan dan mempermudah untuk menampilkan data menggunakan bahasa javascript. Manipulasi response dari bagaimana chatbot pada website dapat menerima pesan dari pengguna, memberikan respons, dan menampilkan respons tersebut dalam antarmuka chatbox.
